// Variables used by Scriptable.
// These must be at the very top of the file. Do not edit.
// icon-color: deep-blue; icon-glyph: chart-line;

class StockWidget {

    constructor() {
        if(args.widgetParameter) {
            this.argumentNotSet = false

            let mappedArgs = args.widgetParameter.split(",");

            let widgetArguments = new Map();


            mappedArgs.forEach((value)=>{
                let keyValue = value.split(":")
                widgetArguments.set(keyValue[0], keyValue[1]);
            })

            if(widgetArguments.has("HID")){
                this.historyInDays = parseInt(widgetArguments.get("HID"));
            }else{
                this.historyInDays = 6
                this.argumentNotSet = true;
            }

            if (widgetArguments.has("CID")){
                this.comdirectId = widgetArguments.get("CID");
            }else{
                this.argumentNotSet = true;
            }
        }else{
            this.historyInDays = 6
            this.argumentNotSet = true
        }

    }

    dataUrl = (id, startDate, endDate)=>`https://www.comdirect.de/inf/kursdaten/historic.csv?DATETIME_TZ_END_RANGE_FORMATED=${endDate}&DATETIME_TZ_START_RANGE_FORMATED=${startDate}&ID_NOTATION=${id}&INTERVALL=16&mask=true&modal=false`

    async run() {
        let data = [120.0, 130.0, 110.0, 90.0, 111.0, 125.99]

        if(this.argumentNotSet === false){
            data = await this.getData();
        }

        let widget = await this.createWidget(data)

        if (!config.runsInWidget) {
            await widget.presentSmall()
        }

        Script.setWidget(widget)
        Script.complete()
    }

    async createWidget(data) {
        // Basic widget setup
        let list = new ListWidget()
        list.setPadding(0, 0, 0, 0)
        let textStack = list.addStack()
        textStack.setPadding(14, 14, 0, 14)
        textStack.layoutVertically()
        textStack.topAlignContent()

        // Header
        let header = textStack.addText(`📈 Fonds - ${this.historyInDays} d`.toUpperCase())
        header.font = Font.mediumSystemFont(13)
        textStack.addSpacer()

        // Main stack for value and area name

        let valueStack = textStack.addStack();
        valueStack.centerAlignContent();
        let numberStack = valueStack.addStack();
        numberStack.layoutVertically();

        valueStack.addSpacer();

        let valuePreviousDay = numberStack.addText(`${data[data.length - 2].toFixed(2)}`)
        valuePreviousDay.font = Font.mediumSystemFont(20)

        let valueCurrentDay = numberStack.addText(`${data[data.length - 1].toFixed(2)}`)
        valueCurrentDay.font = Font.boldSystemFont(22)
        valueCurrentDay.textColor = this.getTrendColor(data);


        // Chip for displaying state data
        let stateStack = valueStack.addStack()
        let trendArrow = stateStack.addText(this.getTrendArrow(data));
        trendArrow.font = Font.boldSystemFont(30)
        trendArrow.textColor = this.getTrendColor(data);
        stateStack.backgroundColor = new Color('888888', .5)
        stateStack.cornerRadius = 40
        stateStack.centerAlignContent();
        stateStack.size = new Size(40, 40);

        textStack.addSpacer()

        // Chart
        let chart = new LineChart(400, 150, data, 30).configure((ctx, path) => {
            ctx.opaque = false;
            ctx.setFillColor(new Color("888888", .5));
            ctx.addPath(path);
            ctx.fillPath(path);
        }).getImage();

        let chartStack = list.addStack()
        chartStack.setPadding(0, 0, 0, 0)
        let img = chartStack.addImage(chart)
        img.applyFittingContentMode()

        return list;

    }

    getTrendArrow(data){
        let lastIndex = data.length - 1

        if(data[lastIndex - 1] < data[lastIndex]){
            return '↑';
        }else if(data[lastIndex - 1] > data[lastIndex]){
            return '↓';
        }

        return "→";
    }

    getTrendColor(data){
        let lastIndex = data.length - 1

        if(data[lastIndex - 1] < data[lastIndex]){
            return Color.green();
        }else if(data[lastIndex - 1] > data[lastIndex]){
            return Color.red();
        }

        return Color.yellow();
    }

    async getData() {
        let csvStrings = await this.getCSVRowsAsArray(); //Array<string>
        return this.processData(csvStrings);
    }

    processData(csvStrings) {
        csvStrings = csvStrings.map((val) => val.replaceAll('\"', ''));
        csvStrings = csvStrings.slice(3);

        let dataArray = [];

        for (let i = 0; i < csvStrings.length; i++) {

            if (csvStrings[i].length < 1) {
                break;
            }

            let dayElements = csvStrings[i].split(";");
            dataArray.push(this.convertToNumber(dayElements[4]));
        }

        return dataArray.reverse();

    }

    async getCSVRowsAsArray() {
        let startDate = this.getDateStringForQuery(this.getDateWithSubtractedDays(this.historyInDays));
        let endDate = this.getDateStringForQuery(this.getDateWithSubtractedDays(0));

        let url = this.dataUrl(this.comdirectId, startDate, endDate);

        console.log(url);

        let req = new Request(url);
        req.method = "GET";
        let data = await req.loadString();
        return data.split("\n");

    }

    convertToNumber(str) {
        return parseFloat(str.replace(',', '.'));
    }

    getDateWithSubtractedDays(subtractedDays){
        let date = new Date()
        date.setDate(date.getDate() - subtractedDays)
        return date;
    }

    getDateStringForQuery(date){
        return `${date.getDate()}.${date.getMonth() + 1}.${date.getFullYear()}`;
    }
}

class LineChart {

    constructor(width, height, values, minValueHeightInset) {
        this.ctx = new DrawContext()
        this.ctx.size = new Size(width, height)
        this.values = values;
        this.minValueHeightInset = minValueHeightInset;
    }

    _calculatePath() {
        let height = this.ctx.size.height - this.minValueHeightInset
        let maxValue = Math.max(...this.values);
        let minValue = Math.min(...this.values);
        let difference = maxValue - minValue;
        let count = this.values.length;
        let step = this.ctx.size.width / (count - 1);
        let points = this.values.map((current, index, all) => {
            let x = step * index
            let y = height - (current - minValue) / difference * height;
            return new Point(x, y)
        });
        return this._getSmoothPath(points);
    }

    _getSmoothPath(points) {
        let path = new Path()
        path.move(new Point(0, this.ctx.size.height));
        path.addLine(points[0]);
        for (var i = 0; i < points.length - 1; i++) {
            let xAvg = (points[i].x + points[i + 1].x) / 2;
            let yAvg = (points[i].y + points[i + 1].y) / 2;
            let avg = new Point(xAvg, yAvg);
            let cp1 = new Point((xAvg + points[i].x) / 2, points[i].y);
            let next = new Point(points[i + 1].x, points[i + 1].y);
            let cp2 = new Point((xAvg + points[i + 1].x) / 2, points[i + 1].y);
            path.addQuadCurve(avg, cp1);
            path.addQuadCurve(next, cp2);
        }
        path.addLine(new Point(this.ctx.size.width, this.ctx.size.height))
        path.closeSubpath()
        return path;
    }

    configure(fn) {
        let path = this._calculatePath()
        if (fn) {
            fn(this.ctx, path);
        } else {
            this.ctx.addPath(path);
            this.ctx.fillPath(path);
        }
        return this.ctx;
    }

}

await new StockWidget().run();