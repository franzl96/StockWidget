# StockWidget

## Beschreibung
Um auf Daten von bestimmten Fonds zuzugreifen um diese in eimem Widget schön visualisiert Darzustellen habe ich ein "Plugin" für Scriptable geschrieben.
Der Code holt sich die Daten aus einem CSV das von Comdirekt generiert wird, wenn man sich die Daten zu seinem Kurs als CSV herunterladen will.

<img width='200' src="Widget.png">

## Scriptable (https://scriptable.app)
Bei Scriptable handelt es sich um eine App für iOS, mit der man Widgets in JavaScript schreiben kann.
Das JavaScript wurde hierbei speziell auf Scriptable zugeschnitten, um Befehle der App zu triggern.
Mehr zu den Befehlen: https://docs.scriptable.app

## Bedienung
<ol>
<li>Importieren des Scriptes in Scriptable</li>
<li>Ein Scriptable Widget auf den Homescreen hinzufügen</li>
<li>Widget bearbeiten auf Homescreen auswählen</li>
<li>Auswählen des richtigen Scriptes</li>
<li>Eingeben der Paramater CID und HID</li>
<ol>
<li>
CID ist die ID die aus der Comdirekt URL gewonnen werden kann (Notation)
https://www.comdirect.de/inf/kursdaten/historic.csv?DATETIME_TZ_END_RANGE_FORMATED=15.09.2022&DATETIME_TZ_START_RANGE_FORMATED=02.09.2022& <b>ID_NOTATION=1234567</b> &INTERVALL=62&mask=true&modal=false
</li>
<li>HID ist die gewünschte Zeitspanne in Tagen in der, der Chart angezeigt werden soll. Also Heute - HID  </li>
</ol>
<li>Bestätigen</li>
</ol>


